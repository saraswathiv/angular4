import { Component, OnInit,Injectable } from '@angular/core';
import {NgForm} from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { FormGroup, FormControl} 
from '@angular/forms';

@Component({
  selector: 'app-routesearch',
  templateUrl: './routesearch.component.html',
  styleUrls: ['./routesearch.component.css']
})
@Injectable()
export class RoutesearchComponent {

  searchForm = new FormGroup({
    from_loc: new FormControl(),
    end_loc:new FormControl()
   });

  data:any =null;
  stops: any = null;  
  stopping_points: any=null;
  constructor(private _http: Http){
    this._http = _http;  
    this.getshuttlestops() ;
  } 
  getshuttlestops()
  {
    return this._http.get('http://localhost:8080/shuttle/stops')
    .map((res: Response) => res.json())
    .subscribe(stops => {
      this.stops = stops;
      //console.log(this.loc);      
    });
  }
  onChange($event)
  {
    let routeid=$event.target.value ;
    return this._http.get('http://localhost:8080/shuttle/route/'+routeid)
    .map((res: Response) => res.json())
    .subscribe(stopping_points => {
      this.stopping_points = stopping_points;
      console.log(this.stopping_points);      
    });
  }

  frmSubmit() {
    //console.log('Name:' + this.loginForm.get('cust_name').value);
    let pickup=this.searchForm.get('from_loc').value;
    let drop=this.searchForm.get('end_loc').value; 
   // console.log(pickup);
    return this._http.get('http://localhost:8080/stops/from/'+pickup+'to/'+drop)
    .map((res: Response) => res.json())
    .subscribe(data => {
      this.data = data;
      console.log(this.data);      
    });
    
  }

}
