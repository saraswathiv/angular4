import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { BookingformComponent } from './bookingform/bookingform.component';
import { RoutesearchComponent } from './routesearch/routesearch.component';

@NgModule({
  declarations: [
    AppComponent,
    BookingformComponent,
    RoutesearchComponent
  ],
  imports: [
    BrowserModule ,
    ReactiveFormsModule,
    HttpModule,
    [AgmCoreModule.forRoot()]
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
