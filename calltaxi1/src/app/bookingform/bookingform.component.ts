import { Component, OnInit,Injectable } from '@angular/core';
import {NgForm} from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { FormGroup, FormControl} 
from '@angular/forms';

@Component({
  selector: 'app-bookingform',
  templateUrl: './bookingform.component.html',
  styleUrls: ['./bookingform.component.css']
})

@Injectable()
export class BookingformComponent { 
  lat: number = 13.0827;
  lng: number = 80.2707;
  
  loginForm = new FormGroup({
    cust_name: new FormControl(),
    pickup_loc:new FormControl(),
    drop_loc: new FormControl(),
    cab_model: new FormControl(),
    cust_mob: new FormControl()

   }); 
  data: any = null;
  loc: any =null;

  constructor(private _http: Http){
    this._http = _http;
    this.getLocation();
  } 
  onSubmit() {
    //console.log('Name:' + this.loginForm.get('cust_name').value);
    let pickup=this.loginForm.get('pickup_loc').value;
    let drop=this.loginForm.get('drop_loc').value;  
    let cab_type= this.loginForm.get('cab_model').value;  
   // console.log(pickup);
    return this._http.get('http://192.168.2.215:8888/cab/cabtype/'+cab_type+'/pick/'+pickup+'/drop/'+drop)
    .map((res: Response) => res.json())
    .subscribe(data => {
      this.data = data;
     // console.log(this.data);      
    });
    
  }

  getLocation()
  {
    return this._http.get('http://192.168.2.215:8888/cab/location')
    .map((res: Response) => res.json())
    .subscribe(loc => {
      this.loc = loc;
      //console.log(this.loc);      
    });
  }


}
