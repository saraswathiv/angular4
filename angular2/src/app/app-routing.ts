/**
 * Created by IT on 10/12/2017.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component'; // main window
import { EmployeeComponent } from './employee/employee.component';
import {EmployeeProfileComponent} from "./employee-profile/employee-profile.component"; // new window/tab

const appRoutes: Routes = [
  { path: 'employee', component: EmployeeComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot( appRoutes )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
