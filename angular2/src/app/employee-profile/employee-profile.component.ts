import {Component, Injectable, OnInit} from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {Router} from "@angular/router";

@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.css']
})
@Injectable()
export class EmployeeProfileComponent {

  data: any = null;

  constructor(private _http: Http, private router: Router) {
    this.getMyBlog();
  }

  private getMyBlog() {
    /*http://localhost:8080/emp/6093
     return this._http.get('https://public-api.wordpress.com/rest/v1.1/sites/oliverveits.wordpress.com/posts/3078')*/
    return this._http.get('http://localhost:8080/emp')
      .map((res: Response) => res.json())
      .subscribe(data => {
        this.data = data;
        console.log(this.data);
      });
  }

  alert(id) {
    /*console.log( id);*/
    window.open( "/employee" );
  }

  navigate() {
    this.router.navigate(["/employee"]);
  }

}
