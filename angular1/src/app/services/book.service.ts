import { Injectable } from '@angular/core';
import { Book } from './book';
import {BOOKS, UserService} from './mock-books';
import {Router} from "@angular/router";
import {Http , Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class BookService {
    getBooks(): Promise<Book[]> {
        return Promise.resolve(BOOKS);
    }
	addBook(book:Book): void {
		this.getBooks().then(books => {
		    let maxIndex = books.length - 1;
			let bookWithMaxIndex = books[maxIndex];
			book.employees_ID = bookWithMaxIndex.employees_ID + 1;
			books.push(book);
		}
		);
    }
	getBook(id: number): Promise<Book> {
        return this.getBooks()
            .then(books => books.find(book => book.employees_ID === id));

    }
	deleteBook(id: number): void {
		this.getBooks().then(books => {
		    let book = books.find(ob => ob.employees_ID === id);
            let bookIndex = books.indexOf(book);
            books.splice(bookIndex, 1);}
		);
  }


}
