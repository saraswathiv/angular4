import { Book } from './book';

export var BOOKS: Book[] = [
 /* {"employees_ID": 1, "name": "Core Java", "price": "25.50", "description": "Core Java Tutorials"},
  {"employees_ID": 2, "name": "Angular", "price": "15.20", "description": "Learn Angular"},
  {"employees_ID": 3, "name": "Hibernate", "price": "13.50", "description": "Hibernate Examples"},
  {"employees_ID": 4, "name": "TypeScript", "price": "26.40", "description": "TypeScript Tutorials"}*/
];



import { Injectable } from '@angular/core';
import {Http, Jsonp, Response} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService  {

  constructor(private http: Http) {
  }

  getUsers(): Observable<Book[]> {
    /*return this.http.get("https://jsonplaceholder.typicode.com/users")*/
    return this.http.get("http://localhost:8080/emp")
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getBookDetails(id: number): void {
    console.log(id);
  }

}
