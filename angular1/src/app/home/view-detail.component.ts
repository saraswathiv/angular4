import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { Book } from '../services/book';
import { BookService } from '../services/book.service';
import {UserService} from "../services/mock-books";

@Component({
    selector: 'view-detail-app',
    templateUrl: './view-detail.component.html',
	styleUrls: ['./view-detail.component.css']
})
export class ViewDetailComponent implements OnInit {
	book: Book = new Book();
  users;
	constructor(private route: ActivatedRoute,
	            private router: Router,
	            private bookService: BookService,
				private userService: UserService,
				private location: Location) {
    this.users = userService.getBookDetails(112);
  }
    ngOnInit(): void {

    }
    goBack(): void {
        this.location.back();
    }
	updateBook(id:number): void {
		this.router.navigate(['/update-book', id]);
	}
}
