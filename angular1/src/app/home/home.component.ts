import { Component, OnInit } from '@angular/core';

import { Book } from '../services/book';
import { BookService } from '../services/book.service';
import {UserService} from "../services/mock-books";

@Component({
    selector: 'home-app',
    templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent {
    books: Book[];
	/*constructor(private bookService: BookService) { }
    getBooks(): void {
        this.bookService.getBooks().then(books => this.books = books);
    }*/
  users;
  constructor(private userService: UserService) {
    this.users = userService.getUsers();
  }
   /* ngOnInit(): void {
        this.getBooks();
    }*/

}
